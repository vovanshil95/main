#define _CRT_SECURE_NO_WARNINGS
#include "image.h"
#include "bmp.h"

#pragma pack(push, 1)
struct bmp_header
{
	uint16_t bfType;
	uint32_t  bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t  biHeight;
	uint16_t  biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t  biClrImportant;
};
#pragma pack(pop)



//void make_bmp_header(struct bmp_header* header, struct image* img) {
//	header->bfType = 19778;
//	header->bfileSize = 54 + img->width * img->height * 3 + img->height * (img->width % 4);
//	header->bfReserved = 0;
//	header->bOffBits = 54;
//	header->biSize = 40;
//	header->biWidth = img->width;
//	header->biHeight = img->height;
//	header->biPlanes = 1;
//	header->biBitCount = 24;
//	header->biCompression = 0;
//	header->biSizeImage = header->bfileSize - header->bOffBits;
//	header->biXPelsPerMeter = 0;
//	header->biYPelsPerMeter = 0;
//	header->biClrUsed = 0;
//	header->biClrImportant = 0;
//}
//


enum read_status from_bmp(FILE* in, struct image* img) {

	struct bmp_header* header;
	char buff[54];

	if (!fread(buff, 54, 1, in)) {
		return READ_INVALID_HEADER;
	}

	header = (struct bmp_header*)buff;

	if (header->biBitCount != 24) {
		return READ_INVALID_BITS;
	}
	if (header->bfType != 19778 || header->bfileSize != 54 + header->biWidth * header->biHeight * 3 + header->biHeight * (header->biWidth % 4) ||
		header->biSizeImage != header->bfileSize - header->bOffBits) {
		return READ_INVALID_SIGNATURE;
	}
	fseek(in, 0L, SEEK_END);
	uint32_t sz = ftell(in);
	if (header->bfileSize != sz) {
		return FILE_SIZE_ERROR;
	}
	rewind(in);
	fseek(in, 54, SEEK_CUR);

	struct pixel* pixel_arr = malloc(sizeof(struct pixel) * header->biWidth * header->biHeight);

	for (uint64_t i = 0; i < header->biHeight; i++) {
		for (size_t j = 0; j < header->biWidth; j++) {
			
			char buff[3];
			fread(buff, 3, 1, in);

			pixel_arr[i * header->biWidth + j] = *(struct pixel*)buff;

		}
		fseek(in, header->biWidth % 4, SEEK_CUR);
	}
	img->height = header->biHeight;
	img->width = header->biWidth;
	img->data = pixel_arr;

	return READ_OK;
}



enum write_status to_bmp(FILE* out, struct image* img) {

	struct bmp_header* header = malloc(sizeof(struct bmp_header));
	header->bfType = 19778;
	header->bfileSize = 54 + img->width * img->height * 3 + img->height * (img->width % 4);
	header->bfReserved = 0;
	header->bOffBits = 54;
	header->biSize = 40;
	header->biWidth = img->width;
	header->biHeight = img->height;
	header->biPlanes = 1;
	header->biBitCount = 24;
	header->biCompression = 0;
	header->biSizeImage = header->bfileSize - header->bOffBits;
	header->biXPelsPerMeter = 0;
	header->biYPelsPerMeter = 0;
	header->biClrUsed = 0;
	header->biClrImportant = 0;

	fwrite(header, 54, 1, out);

	const size_t padding = 0;
	for (size_t i = 0; i < header->biHeight; i++) {
		fwrite(&(img->data[i * header->biWidth]), sizeof(struct pixel), header->biWidth, out);
		fwrite(&padding, sizeof(char), header->biWidth % 4, out);
	}

	free(img->data);
	free(header);

	return WRITE_OK;
}
