#define _CRT_SECURE_NO_WARNINGS
#include "bmp.h"
#include "image.h"
#include <sys/stat.h>

enum open_img_status get_img(const char* source, struct image* img) {

	struct stat buffer;

	if (stat(source, &buffer) != 0) {
		return INVALID_SOURCE;
	}
	FILE* file = fopen(source, "rb");
	if (!file) {
		return CANT_OPEN;
	}
	enum  read_status status = from_bmp(file, img);
	fclose(file);

	if (status != 0) {
		return FROM_BMP_ERROR;
	}
	return OPEN_OK;
	
}

enum save_img_status save_img(const char* source, struct image* img) {

	FILE* file = fopen(source, "wb");
	enum write_status status = to_bmp(file, img);
	if (status != 0) {
		return TO_BMP_ERROR;
	}
	if (fclose(file) != 0) {
		return CANT_CLOSE;
	}
	return SAVE_OK;
}


struct image rotate(struct image source) {

	struct image new_img;
	new_img.width = source.height;
	new_img.height = source.width;
	new_img.data = malloc(sizeof(struct pixel) * new_img.width * new_img.height);

	for (uint64_t i = 0; i < new_img.height; i++) {
		for (uint64_t j = 0; j < new_img.width; j++) {
			uint64_t source_index = source.width * (source.height - 1) - source.width * j  + i;
			uint64_t new_index = i * new_img.width + j;
			new_img.data[new_index] = source.data[source_index];
		}
	}

	return new_img;
}
