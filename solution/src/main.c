﻿#include "image.h"
#include <stdio.h>

int main(int argc, char** argv) {
    (void)argc; (void)argv;

    struct image input;


    enum open_img_status open_status = get_img(argv[1], &input);
    if (open_status == 1) {
        printf("this file doesn't exist");
    }
    else if (open_status == 2) {
        printf("can't work with this format of file");
    }
    else if (open_status == 3) {
        printf("can't open file");
    }
    else {
        struct image output = rotate(input);
        enum save_img_status save_status = save_img(argv[2], &output);
        if (save_status == 1) {
            printf("can't convert image to bmp");
        }
        else if (save_status == 2) {
            printf("can't close file");
        }
    }

}
