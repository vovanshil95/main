#include <stdint.h>

#ifndef __IMAGE_H__ 

#define __IMAGE_H__ 

struct pixel { uint8_t b, g, r; };

struct image {
	uint64_t width, height;
	struct pixel* data;
};

enum open_img_status {
	OPEN_OK = 0,
	INVALID_SOURCE,
	FROM_BMP_ERROR,
	CANT_OPEN
};

enum save_img_status {
	SAVE_OK = 0,
	TO_BMP_ERROR,
	CANT_CLOSE,
};

enum open_img_status get_img(const char* source, struct image* img);

enum save_img_status save_img(const char* source, struct image* img);

struct image rotate(struct image source);

#endif
